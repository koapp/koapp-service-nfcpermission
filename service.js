(function () {
    angular
        .module('king.services.nfcpermission', [])
        .run(loadFunction);

    loadFunction.$inject = ['configService'];

    function loadFunction(configService) {
        // Register upper level modules
        try {
            if (configService.services && configService.services.nfcpermission) {
                askpermissionFunction(configService.services.nfcpermission.scope);
            } else {
                throw "The service is not added to the application";
            }
        } catch (error) {
            console.error("Error", error);
        }
    }

    function askpermissionFunction(scopeData) {
        if (!cordova) return;

        function onError(error) {
            console.error("The following error occurred: " + error);
        }

        cordova.plugins.diagnostic.isNFCPresent(function (present) {
            console.log(present);
            if (present === true) {
                cordova.plugins.diagnostic.registerNFCStateChangeHandler(function (state) {
                    switch (state) {
                        case cordova.plugins.diagnostic.NFCState.UNKNOWN:
                            console.log("NFC state is unknown");
                            break;
                        case cordova.plugins.diagnostic.NFCState.POWERED_OFF:
                            console.log("NFC status is off");
                            navigator.notification.confirm(
                                "The NFC is off. Would you like to switch it on?",
                                function (i) {
                                    if (i == 1) {
                                        cordova.plugins.diagnostic.switchToNFCSettings();
                                    }
                                }, "NFC is off", ['Yes', 'No']);
                            break;
                        case cordova.plugins.diagnostic.NFCState.POWERED_ON:
                            console.log("NFC is powered on");
                            // Yay! use NFC
                            break;
                        default:
                            console.log("In default STATE: " + state);
                    }
                }, onError);
            } else {
                console.log("in else PRESENT: " + present);
            }
        }, onError);
    }
    // --- End servicenameController content ---
})();