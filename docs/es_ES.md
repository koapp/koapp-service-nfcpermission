# Documentación

Este complemento comprueba si el usuario tiene el NFC activado. Si esta desactivado pregunta para activarlo

### Configuración:

Este complemento es plug and play, por lo que no es necesaria ninguna configuración previa
